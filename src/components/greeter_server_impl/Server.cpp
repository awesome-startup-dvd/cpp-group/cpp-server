//
// Created by user on 15.07.21.
//

#include <greeter_server_impl/Server.h>

namespace net
{

    ServerKernel::ServerKernel(std::string ipv4, int port) {
        _port = port;
        _ipv4 = ipv4;
        _server_address = _ipv4 + ":" + std::to_string(_port);
    }

    ServerKernel::ServerKernel() :
        _port(8080),
        _ipv4("127.0.0.1"),
        _server_address(_ipv4 + ":" +  std::to_string(_port)) {
        LOG_INFO("Create default server address: " + _server_address);

    };
    ServerKernel::~ServerKernel() {
        LOG_INFO("Server shutdown.");
    }

    void ServerKernel::start() {

        AuthorizationServiceImpl authorizationService;
        RegistrationServiceImpl registrationService;
        GetUserDataServiceImpl getUserDataService;

        _builder.AddListeningPort(_server_address, grpc::InsecureServerCredentials());
        _builder.RegisterService(&authorizationService);
        _builder.RegisterService(&registrationService);
        _builder.RegisterService(&getUserDataService);

        std::unique_ptr<Server> server(_builder.BuildAndStart());

        LOG_INFO("Server listening on -> " + _server_address);
        LOG_INFO("Server waiting request...");

        server->Wait();
    }

    // ---------------------------------------------------------------------------------------

    Status AuthorizationServiceImpl::LogIn(ServerContext *context, const AuthorizationDataRequest *request,
                                           AuthorizationDataResponse *response)
    {
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        PersonalData* personalData = new PersonalData();
        ServerInfo* serverInfo = new ServerInfo();
        User* user = new User();
        rapidjson::Document document;

        LOG_INFO("-------------> Login in...");

        if(request->login() == "" || request->password() == "") {
            LOG_ERROR("-------------> Empty data from client.");
            serverInfo->set_message("[SERVER] -> Error: login or password is empty.");
            response->set_isauth(false);
            response->set_allocated_msg(serverInfo);
            return Status::OK;
        }

        auto result = collection.find_one(bsoncxx::builder::stream::document{}
                                        << "login" << request->login()
                                        << "password" << request->password()
                                        << bsoncxx::builder::stream::finalize);

        if(result) {

            document.Parse(bsoncxx::to_json(result->view()).c_str());
            personalData->set_name(document["personal_data"]["name"].GetString());
            personalData->set_surname(document["personal_data"]["surname"].GetString());
            personalData->set_patronymic(document["personal_data"]["patronymic"].GetString());
            personalData->set_yearsold(document["personal_data"]["yearsold"].GetString());

            user->set_login(document["login"].GetString());
            user->set_password(document["password"].GetString());
            user->set_access_lvl((UserApi::User_AccessLvl)document["access_lvl"].GetInt64());
            user->set_allocated_personal_data(personalData);

            response->set_allocated_user(user);
            response->set_isauth(true);

            LOG_INFO("------------> Success authorization!");
            serverInfo->set_message("[SERVER] -> Success authorization.");

        } else {
            LOG_ERROR("-------------> Canceled login in, user not found!");
            serverInfo->set_message("[SERVER] -> Error: Canceled login in, user not found");
            response->set_isauth(false);
            response->set_allocated_msg(serverInfo);
            return Status::OK;
        }

        response->set_allocated_msg(serverInfo);

        return Status::OK;
    }

    Status RegistrationServiceImpl::RegistrationNewUser(ServerContext *context, const RegistrationDataRequest *request,
                               RegistrationDataResponse *response)
    {
        ServerInfo* serverInfo = new ServerInfo();
        User* user_impl = new User();
        PersonalData* personalData = new PersonalData();

        LOG_INFO("-------------> Start registration... ");
        user::PersonalData pd(&request->user_reg_data().personal_data());
        user::User usr(&request->user_reg_data());

        if(usr.AddInDb() == true) {
            serverInfo->set_message("[SERVER] -> Success register " +
                    request->user_reg_data().personal_data().name());
        } else {
            serverInfo->set_message("[SERVER] -> Error: Canceled register, user already exist");
            response->set_allocated_msg(serverInfo);
            response->set_isregister(false);
            return Status::OK;
        }


        personalData->set_name(usr.get_personalData()->_name);
        personalData->set_surname(usr.get_personalData()->_surname);
        personalData->set_patronymic(usr.get_personalData()->_patronymic);
        personalData->set_yearsold(usr.get_personalData()->_yearsold);

        user_impl->set_login(usr.get_login());
        user_impl->set_password(usr.get_pass());
        user_impl->set_access_lvl((UserApi::User_AccessLvl)usr.get_access_lvl());
        user_impl->set_allocated_personal_data(personalData);

        response->set_allocated_user(user_impl);
        response->set_allocated_msg(serverInfo);
        response->set_isregister(true);

        LOG_INFO("-------------> Complete registration!");

        return Status::OK;
    }

    Status GetUserDataServiceImpl::GetUserData(ServerContext *context, const GetUserDataRequest *request,
                                               GetUserDataResponse *response)
    {
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        User* userImpl = new User();
        PersonalData* personalData = new PersonalData();
        rapidjson::Document document;

        mongocxx::cursor cursor = collection.find({});
        for(auto& it : cursor)
        {
            document.Parse(bsoncxx::to_json(it).c_str());

            if (document["user"]["login"].GetString() == request->login() &&
                document["user"]["password"].GetString() == request->password()) {

                auto userObject = document["user"].GetObject();
                personalData->set_name(userObject["personal_data"]["name"].GetString());
                personalData->set_surname(userObject["personal_data"]["surname"].GetString());
                personalData->set_patronymic(userObject["personal_data"]["patronymic"].GetString());
                personalData->set_yearsold(userObject["personal_data"]["yearsold"].GetString());

                userImpl->set_id(1);//TODO fix hardcode
                userImpl->set_imagesource(userObject["image_source"].GetString());
                userImpl->set_access_lvl((UserApi::User_AccessLvl)userObject["access_lvl"].GetInt());

                rapidjson::Document arrayContactsForJson;
                arrayContactsForJson.SetObject();
                arrayContactsForJson.AddMember("contacts", userObject["contacts"].GetArray(),bd::jsonAlloc);
                userImpl->set_array_contacts(bd::WriteDocumentToString(arrayContactsForJson));

                userImpl->set_allocated_personal_data(personalData);
                response->set_allocated_user(userImpl);

                LOG_INFO("-------------> User [" + request->login() + "] was found!");
                return Status::OK;
            }
        }

        LOG_ERROR("-------------> User not found... ");
        return Status::OK;
    }
}

