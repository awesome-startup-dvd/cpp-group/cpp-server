//
// Created by root on 16.08.21.
//

#include <user.h>

namespace user {

    User::User(std::string imageSource,
               std::string login,
               std::string pass,
               std::vector<Contacts> contacts,
               AccessLvl accessLvl,
               PersonalData personalData)
    {
        _imageSource = imageSource;
        _login = login;
        _password = pass;
        _accessLvl = accessLvl;
        _personalData = new PersonalData(personalData);
        _contacts = contacts;
    }

    User::User(const UserApi::User* user) {
        _login = user->login();
        _password = user->password();
        _accessLvl = (AccessLvl)user->access_lvl();
        _personalData = new PersonalData(&user->personal_data());
    }

    User::User(User &user) {
        _login = user._login;
        _password = user._password;
        _accessLvl = user._accessLvl;
        _personalData = user._personalData;
    }

    User::User(const User &user) {
        _login = user._login;
        _password = user._password;
        _accessLvl = user._accessLvl;
        _personalData = user._personalData;
    }

// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------

    rapidjson::Document User::to_json(){
        rapidjson::Document document;
        document.SetObject();
        rapidjson::Value jsonUser(rapidjson::kObjectType);
        rapidjson::Value login;
        rapidjson::Value password;
        rapidjson::Value imageSource;
        rapidjson::Value jsonContacts(rapidjson::kArrayType);
        rapidjson::Value accessLvl;

        for (int i = 0; i < _contacts.size(); ++i) {
            jsonContacts.PushBack(_contacts[i].to_json(), bd::jsonAlloc);
        }

        login.SetString(_login.c_str(), _login.size(),bd::jsonAlloc);
        password.SetString(_password.c_str(), _password.size(),bd::jsonAlloc);
        imageSource.SetString(_imageSource.c_str(), _imageSource.size(),bd::jsonAlloc);
        accessLvl.SetInt(static_cast<int>(_accessLvl));


        jsonUser.AddMember("login", login,bd::jsonAlloc);
        jsonUser.AddMember("password", password,bd::jsonAlloc);
        jsonUser.AddMember("image_source", imageSource,bd::jsonAlloc);
        jsonUser.AddMember("contacts", jsonContacts,bd::jsonAlloc);
        jsonUser.AddMember("access_lvl", accessLvl,bd::jsonAlloc);
        jsonUser.AddMember("personal_data", _personalData->to_json()["personal_data"].GetObject(), bd::jsonAlloc);

        document.AddMember("user", jsonUser, bd::jsonAlloc);
        return document;
    }

    rapidjson::Document PersonalData::to_json() {
        rapidjson::Document document;
        document.SetObject();
        rapidjson::Value jsonPersonalData(rapidjson::kObjectType);
        rapidjson::Value name;
        rapidjson::Value surname;
        rapidjson::Value patronymic;
        rapidjson::Value yearsold;

        name.SetString(_name.c_str(), _name.size(),bd::jsonAlloc);
        surname.SetString(_surname.c_str(), _surname.size(),bd::jsonAlloc);
        patronymic.SetString(_patronymic.c_str(), _patronymic.size(),bd::jsonAlloc);
        yearsold.SetString(_yearsold.c_str(), _yearsold.size(),bd::jsonAlloc);

        jsonPersonalData.AddMember("name", name,bd::jsonAlloc);
        jsonPersonalData.AddMember("surname", surname,bd::jsonAlloc);
        jsonPersonalData.AddMember("patronymic", patronymic,bd::jsonAlloc);
        jsonPersonalData.AddMember("yearsold", yearsold,bd::jsonAlloc);

        document.AddMember("personal_data", jsonPersonalData, bd::jsonAlloc);
        return document;
    }

    rapidjson::Document Contacts::to_json() {
        rapidjson::Document document;
        document.SetObject();
        rapidjson::Value jsonContact(rapidjson::kObjectType);
        rapidjson::Value username;
        rapidjson::Value imageSource;
        rapidjson::Value lastMessage;
        rapidjson::Value jsonMessages(rapidjson::kArrayType);
        rapidjson::Value messages(rapidjson::kArrayType);

        for (int i = 0; i < _messages.size(); ++i) {
            messages.PushBack(_messages[i].to_json(),bd::jsonAlloc);
        }

        username.SetString(_username.c_str(), _username.size(),bd::jsonAlloc);
        imageSource.SetString(_imageSource.c_str(), _imageSource.size(),bd::jsonAlloc);
        lastMessage.SetString(_lastMessage.c_str(), _lastMessage.size(),bd::jsonAlloc);

        jsonContact.AddMember("username", username ,bd::jsonAlloc);
        jsonContact.AddMember("image_source", imageSource,bd::jsonAlloc);
        jsonContact.AddMember("last_message", lastMessage,bd::jsonAlloc);
        jsonContact.AddMember("messages", messages,bd::jsonAlloc);

        document.AddMember("contact", jsonContact, bd::jsonAlloc);
        return document;
    }

    rapidjson::Document Message::to_json() {
        rapidjson::Document document;
        document.SetObject();
        rapidjson::Value jsonMessage(rapidjson::kObjectType);
        rapidjson::Value login;
        rapidjson::Value loginColor;
        rapidjson::Value imageSource;
        rapidjson::Value message;
        rapidjson::Value time;
        rapidjson::Value firstMessage;

        login.SetString(_login.c_str(), _login.size(),bd::jsonAlloc);
        loginColor.SetString(_loginColor.c_str(), _loginColor.size(),bd::jsonAlloc);
        imageSource.SetString(_imageSource.c_str(), _imageSource.size(),bd::jsonAlloc);
        message.SetString(_message.c_str(), _message.size(),bd::jsonAlloc);
        time.SetString(_time.c_str(), _time.size(),bd::jsonAlloc);
        firstMessage.SetBool(_firstmessage);

        jsonMessage.AddMember("login", login ,bd::jsonAlloc);
        jsonMessage.AddMember("login_color", loginColor,bd::jsonAlloc);
        jsonMessage.AddMember("image_source", imageSource,bd::jsonAlloc);
        jsonMessage.AddMember("message", message,bd::jsonAlloc);
        jsonMessage.AddMember("time", time,bd::jsonAlloc);
        jsonMessage.AddMember("first_message", firstMessage,bd::jsonAlloc);

        document.AddMember("message", jsonMessage, bd::jsonAlloc);

        return document;
    }

// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------

    void User::show_data() {
        std::cout << "\n-------------SHOW USER DATA----------------" << std::endl;
        std::cout << "Login: " << _login << "\n";
        std::cout << "Password: " << _password << "\n";
        std::cout << "Access lvl: " << _accessLvl << "\n";
        std::cout << "Name: " << _personalData->_name << "\n";
        std::cout << "Surname: " << _personalData->_surname << "\n";
        std::cout << "Patronymic: " << _personalData->_patronymic << "\n";
        std::cout << "Years old: " << _personalData->_yearsold << "\n";
        std::cout << "--------------------------------------------" << "\n\n";
    }

// --------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------

    bool User::AddInDb() {
        LOG_INFO("Data from client:");
        this->show_data(); //DEBUG TODO: delete maybe...
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        auto user_json = bsoncxx::from_json(bd::WriteDocumentToString(this->to_json())); //TODO optimizate do func for this operation
        auto result = collection.find(user_json.view());

        if(result.begin() != result.end()){
            LOG_ERROR("-------------> Canceled user already exist!");
            return false; // If user not unique
        }

        collection.insert_one(user_json.view());
        return true;
    }

    bool User::RemoveFromDb() { //TODO NEED TEST
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        auto user_json = bsoncxx::from_json(bd::WriteDocumentToString(this->to_json())); //TODO optimizate do func for this operation
        auto result = collection.find(user_json.view());

        if(result.begin() == result.end())
            return false; // User not found

        collection.delete_one(user_json.view());
        return true;
    }

    bool UpdateInDb(User& current_usr, User& update_usr) { //TODO NEED TEST
        mongocxx::collection collection = bd::Storage::get_instance()->get_db()[bd::kCollectionName.c_str()];
        auto builder = bsoncxx::builder::stream::document{};
        auto current_usr_bson= bsoncxx::from_json(bd::WriteDocumentToString(current_usr.to_json()));
        auto update_usr_bson = bsoncxx::from_json(bd::WriteDocumentToString(update_usr.to_json()));

        auto result_curr_usr = collection.find(current_usr_bson.view());
        auto result_upd_usr = collection.find(update_usr_bson.view());

        if(result_curr_usr.begin() == result_curr_usr.end()) {
            LOG_ERROR("-------------> Canceled update, user not found!");
            return false; // User not found
        }

        if(result_upd_usr.begin() != result_upd_usr.end()) {
            LOG_ERROR("-------------> Canceled update, user already exist!");
            return false; //User already exist
        }

        collection.update_one(current_usr_bson.view(),
        builder << "$set"
        << update_usr_bson
        << bsoncxx::builder::stream::finalize);

        return true;
    }


    PersonalData* User::get_personalData() {
        return _personalData;
    }

    std::string User::get_login() {
        return _login;
    }

    std::string User::get_pass() {
        return _password;
    }

    AccessLvl User::get_access_lvl() {
        return _accessLvl;
    }

    int User::get_id() {
        return _id;
    }
}
