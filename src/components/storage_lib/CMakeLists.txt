#---------------------SET COMPONENT NAME---------------------------------------#
set(LIB_NAME storage_lib)
#------------------------------------------------------------------------------#
#---------------------SET LOCAL NAME TO LINK COMPONENT-------------------------#
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")
set(LOGGER_LIB logger)
#------------------------------------------------------------------------------#
#-----------------------INCLUDE HEADER-----------------------------------------#
file(GLOB HEADER_LIST CONFIGURE_DEPENDS
        "${PROJECT_SOURCE_DIR}/includes/components/storage_lib/*.h")
list(APPEND CMAKE_PREFIX_PATH "/opt/grpc" "/opt/protobuf")
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

#------------------------------------------------------------------------------#
#---------------------SET PROTO FILE-------------------------------------------#
set(PROTOS
        ${PROJECT_SOURCE_DIR}/protos/message_kernel.proto
        )

set(PROTO_SRC_DIR ${CMAKE_CURRENT_BINARY_DIR}/proto-src)
file(MAKE_DIRECTORY ${PROTO_SRC_DIR})
#------------------------------------------------------------------------------#
#---------------------FIND PACKAGES--------------------------------------------#
find_package(Threads REQUIRED)
find_package(Protobuf REQUIRED)
find_package(GRPC REQUIRED)
include_directories(${PROTO_SRC_DIR})
#------------------------------------------------------------------------------#
#---------------------GENERATE AUTOCODE----------------------------------------#
protobuf_generate_cpp(PROTO_SRCS PROTO_HDRS ${PROTO_SRC_DIR} ${PROTOS})
grpc_generate_cpp(GRPC_SRCS GRPC_HDRS ${PROTO_SRC_DIR} ${PROTOS})

#------------------------------------------------------------------------------#
#------------------------------------------------------------------------------#
if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Debug)
endif()
if (CMAKE_BUILD_TYPE MATCHES Debug)
    find_package(mongocxx REQUIRED)
    find_package(bsoncxx REQUIRED)
elseif (CMAKE_BUILD_TYPE MATCHES Release)
    find_package(libmongocxx REQUIRED)
    find_package(libbsoncxx REQUIRED)
endif ()
#------------------------------------------------------------------------------#
#---------------------ADD SOURCE-----------------------------------------------#
set(SOURCE storage.cpp
           user.cpp)
#------------------------------------------------------------------------------#
#---------------------ADD LIBRARY----------------------------------------------#
add_library(${LIB_NAME} ${SOURCE}
        ${HEADER_LIST}
        ${PROTO_SRCS}
        ${GRPC_SRCS})
#------------------------------------------------------------------------------#
#--------------------INCLUDE DIRS----------------------------------------------#
target_include_directories(${LIB_NAME} PUBLIC
        ${PROJECT_SOURCE_DIR}/includes/components/storage_lib)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Debug)
endif()

if (CMAKE_BUILD_TYPE MATCHES Debug)
    include_directories(${LIBMONGOCXX_INCLUDE_DIR})
    include_directories(${LIBBSONCXX_INCLUDE_DIR})
elseif (CMAKE_BUILD_TYPE MATCHES Release)
    target_include_directories (${LIB_NAME} PRIVATE /usr/local/include/mongocxx/v_noabi;
            /usr/local/include/bsoncxx/v_noabi
    ;/usr/local/include/libmongoc-1.0
    ;/usr/local/include/libbson-1.0)
endif ()
#---------------------LINK COMPONENTS------------------------------------------#
target_link_libraries(${LIB_NAME} PUBLIC ${LOGGER_LIB}
                        gRPC::grpc++_reflection
                        protobuf::libprotobuf)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE Debug)
endif()

if (CMAKE_BUILD_TYPE MATCHES Debug)
    target_link_libraries(${LIB_NAME} PUBLIC mongo::bsoncxx_shared)
    target_link_libraries(${LIB_NAME} PUBLIC mongo::mongocxx_shared)
elseif (CMAKE_BUILD_TYPE MATCHES Release)
    target_link_libraries (${LIB_NAME} PRIVATE /usr/local/lib/libmongocxx.so;
            /usr/local/lib/libbsoncxx.so)
endif ()
#------------------------------------------------------------------------------#
target_compile_features(${LIB_NAME} PUBLIC cxx_std_17)
source_group(TREE "${PROJECT_SOURCE_DIR}/includes/components/storage_lib"
        PREFIX "Header Files"
        FILES ${HEADER_LIST})
#------------------------------------------------------------------------------#
