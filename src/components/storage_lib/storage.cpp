//
// Created by root on 12.08.21.
//

#include <storage.h>

namespace bd {

    using bsoncxx::builder::stream::close_array;
    using bsoncxx::builder::stream::close_document;
    using bsoncxx::builder::stream::document;

    using bsoncxx::builder::stream::finalize;
    using bsoncxx::builder::stream::open_array;
    using bsoncxx::builder::stream::open_document;

    std::string kDatabaseName = "StorageMongo";
    std::string kCollectionName = "Users";
    std::string kMongoDbUri = "mongodb://127.0.0.1:27017";

    rapidjson::MemoryPoolAllocator<> jsonAlloc;

    Storage* Storage::p_instance = 0;

    struct Storage::mongo_conn {
        mongo_conn() {
            mongocxx::instance inst{};
            uri = {kMongoDbUri.c_str()};
            client = {uri};
            db = {client[kDatabaseName.c_str()]};
        }
        mongocxx::uri uri;
        mongocxx::client client;
        mongocxx::database db;
    };

    Storage::Storage()
    : mongo_conn_ptr(std::make_unique<mongo_conn>())
    {
        LOG_INFO("Connect to database -> " + kMongoDbUri);
    }

    mongocxx::database Storage::get_db() {
        return mongo_conn_ptr->db;
    }

    std::string WriteDocumentToString(const rapidjson::Document& document) {
        rapidjson::StringBuffer buffer;
        rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
        document.Accept(writer);
        return buffer.GetString();
    }
}

