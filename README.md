
Server - kernel server of xProject
=====================
---------------------
Environmental requirements
---------------------
* boost >= 1.68
* cmake >= 3.10
* GCC >= 9.3


Components
---------------------

Component name                  | Description
--------------------------------|-------------------------------------------------------------------------------------
Server                          | server for handling message from xProject 


Build
---------------------
```bash
git clone https://gitlab.com/awesome-startup-dvd/cpp-group/cpp-server.git
cd cpp-server
mkdir bin && cd bin
cmake .. 
make -j4
```

### TODO
* [X] Responce request simple kernal server
* [X] Grpc functional
* [X] Debugger\Logger
* [ ] Registration\Authorization
* [ ] Database
* [ ] ...
