#include <greeter_server_impl/Server.h>

void tests()
{
    user::Message msg1("Admin", "Green", "link.com", "message bla bla bla", "11:24", true);
    user::Message msg2("Admin", "Green", "link.com", "message bla bla bla", "11:25", true);
    user::Message msg3("Admin", "Green", "link.com", "message bla bla bla", "11:26", true);

    std::vector<user::Message> messages;
    messages.push_back(msg1);
    messages.push_back(msg2);
    messages.push_back(msg3);

    user::Contacts cont1("Lexa", "image.com", "bla bla bla", messages);
    user::Contacts cont2("Stepan", "image.com", "bla bla bla", messages);
    user::Contacts cont3("Oleg", "image.com", "bla bla bla", messages);

    std::vector<user::Contacts> contacts;
    contacts.push_back(cont1);
    contacts.push_back(cont2);
    contacts.push_back(cont3);

    user::PersonalData pd("Dima","Matsiukhov","Ivanovich","11.11.2000");
    user::PersonalData pd2("Lox","Lox","Lox","11.11.2000");
    user::User test("image.com", "Admin", "Admin", contacts, user::HIGH, pd);
    user::User test2("lol.com", "NeAdmin", "NeAdmin", contacts, user::HIGH, pd2);
    user::UpdateInDb(test,test2);
}

int main(int argc, char** argv) {
    bd::Storage::get_instance();
    net::ServerKernel serverKernel("25.84.177.154", 50051);
    serverKernel.start();
    return 0;
}
