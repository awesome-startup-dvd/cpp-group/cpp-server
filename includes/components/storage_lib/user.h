//
// Created by root on 16.08.21.
//

#ifndef CPP_SERVER_USER_H
#define CPP_SERVER_USER_H

#include <iostream>
#include <string>
#include <storage.h>
#include <rapidjson/document.h>

namespace user {

    enum AccessLvl {
        LOW = 0,
        AVERAGE = 1,
        HIGH = 2
    };

    class Message {
    public:
        Message(std::string login,
                std::string loginColor,
                std::string imageSource,
                std::string message,
                std::string time,
                bool        firstmessage)
                : _login(login),
                _loginColor(loginColor),
                _imageSource(imageSource),
                _message(message),
                _time(time),
                _firstmessage(firstmessage){};
    public:
        rapidjson::Document to_json();
    private:
        std::string _login;
        std::string _loginColor;
        std::string _imageSource;
        std::string _message;
        std::string _time;
        bool        _firstmessage;
    };

    class Contacts {
    public:
        Contacts(std::string username, std::string imageSource, std::string lastMessage, std::vector<Message> messages)
        : _username(username),
        _imageSource(imageSource),
        _lastMessage(lastMessage),
        _messages(messages) {};
    public:
        rapidjson::Document to_json();

    private:
        std::string _username;
        std::string _imageSource;
        std::string _lastMessage;
        std::vector<Message> _messages;
    };

    class PersonalData {
    public:
        PersonalData(std::string name, std::string surname, std::string patronumic, std::string yearsold)
        : _name(name),
          _surname(surname),
          _patronymic(patronumic),
          _yearsold(yearsold) {};

        PersonalData(const PersonalData& pd)
        : _name(pd._name),
          _surname(pd._surname),
          _patronymic(pd._patronymic),
          _yearsold(pd._yearsold) {};

        PersonalData(const UserApi::PersonalData* personalData)
        : _name(personalData->name()),
          _surname(personalData->surname()),
          _patronymic(personalData->patronymic()),
          _yearsold(personalData->yearsold()) {};

        PersonalData() {};
    public:
        rapidjson::Document to_json();

    public:
        std::string _name;
        std::string _surname;
        std::string _patronymic;
        std::string _yearsold;
    };

    class User : public bd::StorageHandler {
        public:
            User(std::string imageSource,
                 std::string login,
                 std::string pass,
                 std::vector<Contacts> contacts,
                 AccessLvl accessLvl,
                 PersonalData personalData);
            User(const UserApi::User* user);
            User(const User &user);
            User(User &user);

        public:
            bool AddInDb() override;
            bool RemoveFromDb() override;

            rapidjson::Document to_json();
            PersonalData* get_personalData();
            AccessLvl get_access_lvl();
            std::string get_login();
            std::string get_pass();
            void show_data();
            int get_id();

        private:
            int _id;
            std::string _imageSource;
            std::string _login;
            std::string _password;
            std::vector<Contacts> _contacts;
            AccessLvl _accessLvl;
            PersonalData* _personalData;
        };

    bool UpdateInDb(User& current_usr, User& update_usr);
}

#endif //CPP_SERVER_USER_H
