//
// Created by root on 12.08.21.
//

#ifndef CPP_SERVER_STORAGE_H
#define CPP_SERVER_FAKE_DATABASE_H

#include "message_kernel.grpc.pb.h"
#include <cstdint>
#include <iostream>
#include <vector>
#include <string>

#include <bsoncxx/json.hpp>
#include <mongocxx/client.hpp>
#include <mongocxx/stdx.hpp>
#include <mongocxx/uri.hpp>
#include <mongocxx/instance.hpp>
#include <bsoncxx/builder/stream/helpers.hpp>
#include <bsoncxx/builder/stream/document.hpp>
#include <bsoncxx/builder/stream/array.hpp>

#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include <local_logger.h>

namespace bd {
    extern std::string kDatabase;
    extern std::string kCollectionName;
    extern std::string kMongoDbUri;

    extern rapidjson::MemoryPoolAllocator<> jsonAlloc;

    class Storage {
    public:
        static Storage * get_instance() {
            if(!p_instance)
                p_instance = new Storage();
            return p_instance;
        }
        mongocxx::database get_db();
    private:
        Storage();
        static Storage* p_instance;
        struct mongo_conn;
        std::unique_ptr<mongo_conn> mongo_conn_ptr;
    };

    class StorageHandler {
    public:
        virtual bool AddInDb() = 0;
        virtual bool RemoveFromDb() = 0;
    };

    std::string WriteDocumentToString(const rapidjson::Document& document);
}

#endif //CPP_SERVER_STORAGE_H
